# External Content

Allows Drupal to reference, consume & display data from external 
JSONAPI sources. 

Items can be selected by title or by taxonomy term. Sources must first be 
configured via the config page. The sources available for selection in any 
given field can be limited in the field settings.

The ExternalContentJsonApi.php class provides some useful methods to extract 
data from the returned JSONAPI.

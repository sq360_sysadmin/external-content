<?php

namespace Drupal\external_content;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an external_content_source entity type.
 */
interface ExternalContentSourceInterface extends ConfigEntityInterface {

}
